# Angular Scope Helper
This is a small script to help you out when you are once again wondering if there is already an existing scope for your commit.

## Requirements
```python3```

## Run
Add the script to your path, i.e.

```ln -s /path/to/script /usr/local/bin```

Make sure the file is executable

```chmod +x /path/to/script```

Then navigate to a git repository and execute `angular-scope-helper` anywhere within the git repo.

Alternatively execute within a git repo with python3

```python3 /path/to/script```
