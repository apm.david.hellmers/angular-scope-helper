#!/usr/bin/env python3

import subprocess
import re

def extractScope(line):
    return line.partition('(')[2].partition(')')[0]

def main():
    result = subprocess.run(['git', '--no-pager', 'log', '--oneline'], stdout=subprocess.PIPE)
    if result:
        commits = result.stdout.decode('utf-8').split('\n')
        # print(commits)
        # scopes = set([extractScope(line) for line in commits if re.match(r"\A(\w|\s)+\(\w+\):.*", line)])
        scopes = [extractScope(line) for line in commits if re.search(r"\(.+\):", line)]
        for line in sorted(set(scopes)):
            print(line)

if __name__ == "__main__":
    main()
